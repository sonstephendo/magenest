<?php
/**
 * OddEven
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Ui\Component\Listing\Column;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Sales\Api\OrderRepositoryInterface;

class OddEven extends Column
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        array $components = [],
        array $data = []
    ) {
        $this->orderRepository = $orderRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        
    }
    
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                
                $order = $this->orderRepository->get($item['entity_id']);
                $id = $order->getData('entity_id');
                
                if ($id % 2) {
                    //odd
                    $class = 'grid-severity-critical';
                    $value = 'Odd';
                } else {
                    //even
                    $class = 'grid-severity-notice';
                    $value = 'Even';
                }
                $style = '<span	class="' . $class . '"><span>' . $value . '</span></span>';
                $item['odd_even'] = html_entity_decode($style);
            }
        }
        return $dataSource;
    }
}
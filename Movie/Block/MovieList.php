<?php

namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Movie;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

use Magenest\Movie\Model\MovieActorFactory;
use Magenest\Movie\Model\MovieFactory;
use Magenest\Movie\Model\ActorFactory;
use Magenest\Movie\Model\DirectorFactory;


class MovieList extends Template
{
    protected $_movieFactory;
    protected $_actorFactory;
    protected $_directorFactory;
    protected $_movieActorFactory;
    //TODO fix this
    //protected $_collection;

    /**
     * Construct
     * @param Context           $context
     * @param MovieFactory      $movieFactory
     * @param ActorFactory      $actorFactory
     * @param DirectorFactory   $directorFactory
     * @param MovieActorFactory $movieActorFactory
     * @param array             $data
     */
    public function __construct(
        Context $context,
        MovieFactory $movieFactory,
        ActorFactory $actorFactory,
        DirectorFactory $directorFactory,
        MovieActorFactory $movieActorFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_movieFactory = $movieFactory;
        $this->_actorFactory = $actorFactory;
        $this->_directorFactory = $directorFactory;
        $this->_movieActorFactory = $movieActorFactory;
    }


    public function getList()
    {
//		$tbName = $this->_movieFactory->create()
//			->getResource()
//			->getTable('magenest_movie');
        $collection = $this->_movieFactory->create()->getCollection();
        $items = $collection->load();
        return $items;
    }

    public function getActors(Movie $movie)
    {
        $movieId = $movie->getId();
        $results = [];
        $data = $this->getJoinCollection()->getData();
        foreach ($data as $item) {
            if ($item['movie_id'] === $movieId) {
                $temp['actor_id'] = $item['actor_id'];
                $temp['actor_name'] = $item['actor_name'];
                array_push($results, $temp);
            }
        }
        return $results;
    }

    public function getDirectorName(Movie $movie)
    {
        $directorID = $movie->getDirectorId();
        $collection = $this->_directorFactory->create()->getCollection();
        $data = $collection->getData();
        foreach ($data as $item) {
            if ($item['director_id'] === $directorID) {
                return $item['name'];
            }
        }
        return null;
    }

    public function getJoinCollection()
    {
        $tbName = $this->_actorFactory->create()
            ->getResource()
            ->getTable('magenest_actor');

        $movieTb = $this->_actorFactory->create()
            ->getResource()
            ->getTable('magenest_movie');

        $collection = $this->_movieActorFactory->create()->getCollection();
        $collection->getSelect()
            ->joinLeft(
                ['ma' => $tbName],
                'main_table.actor_id = ma.actor_id',
                ['*', 'ma.name as actor_name']
            )->joinLeft(
                ['mm' => $movieTb],
                'main_table.movie_id = mm.movie_id'
            );
        return $collection;
    }
}
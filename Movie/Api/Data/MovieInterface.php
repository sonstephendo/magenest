<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 28/06/2018
 * Time: 00:04
 */

namespace Magenest\Movie\Api\Data;


interface MovieInterface
{
	const NAME = 'name';
	const CREATION_TIME = 'created_at';
	const UPDATE_TIME = 'updated_at';

	public function getId();

	public function getName();

	public function getCreationTime();

	public function getUpdateTime();

//	public function setId($id);

//	public function setName($name);

//	public function setCreationTime($creationTime);

//	public function setUpdateTime($updateTime);

}
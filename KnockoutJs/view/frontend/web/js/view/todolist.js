/**
 * todolist
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
define([
    'uiComponent',
    'ko',
    'jquery',
    'Magenest_KnockoutJs/js/model/ToDoList'
], function (Component, ko, $, ToDoList) {
    'use strict';

    var self;
    return Component.extend({
        initialize: function () {
            self = this;
            this._super();
        },
        tasks: ToDoList.tasks,
        task: ToDoList.task,
        addTask: ToDoList.addTask,
        clearTask: ToDoList.clearTask,
        deleteTask: ToDoList.deleteTask,
        completeTask: ToDoList.completeTask,
        sortByPriority: ToDoList.sortByPriority,
        sortByName: ToDoList.sortByName,
	    numOfCompletedTasks: ToDoList.numOfCompletedTasks,
    });
});
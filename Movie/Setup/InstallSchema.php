<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 08:24
 */

namespace Magenest\Movie\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

	/**
	 * Installs DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		$this->createDirectorTable($installer);
		$this->createActorTable($installer);
		$this->createMovieTable($installer);
		$this->createMovieActorTable($installer);

		$installer->endSetup();
	}

	private function createMovieTable(SchemaSetupInterface $installer)
	{
		try {
			if (!$installer->tableExists('magenest_movie')) {
				$table = $installer->getConnection()->newTable(
					$installer->getTable('magenest_movie')
				)
					->addColumn(
						'movie_id',
						Table::TYPE_INTEGER,
						null,
						[
							'primary' => true,
							'identity' => true,
							'nullable' => false,
							'unsigned' => true,
						],
						'Movie ID'
					)->addColumn(
						'name',
						Table::TYPE_TEXT,
						255,
						['nullable => false'],
						'Movie Name'
					)->addColumn(
						'description',
						Table::TYPE_TEXT,
						255,
						['nullable => false'],
						'Movie Description'
					)->addColumn(
						'rating',
						Table::TYPE_INTEGER,
						10,
						['nullable => false'],
						'Movie Rating'
					)->addColumn(
						'director_id',
						Table::TYPE_INTEGER,
						null,
						[
							'nullable' => false,
							'unsigned' => true,
						],
						'Direction Id'
					)->addColumn(
						'created_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
						'Created At'
					)->addColumn(
						'updated_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
						'Updated At'
					)->addForeignKey(
						"fk_movie_director",
						"director_id",
						"magenest_director",
						"director_id"
					)->setComment('Movie Table');
				$installer->getConnection()->createTable($table);
			}
		} catch (\Zend_Db_Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}
	}

	private function createDirectorTable(SchemaSetupInterface $installer)
	{
		try {
			if (!$installer->tableExists('magenest_director')) {
				$table = $installer->getConnection()->newTable(
					$installer->getTable('magenest_director')
				)
					->addColumn(
						'director_id',
						Table::TYPE_INTEGER,
						null,
						[
							'primary' => true,
							'identity' => true,
							'nullable' => false,
							'unsigned' => true,
						],
						'Director ID'
					)->addColumn(
						'name',
						Table::TYPE_TEXT,
						255,
						['nullable => false'],
						'Director Name'
					)->addColumn(
						'created_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
						'Created At'
					)->addColumn(
						'updated_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
						'Updated At'
					)->setComment('Director Table');
				$installer->getConnection()->createTable($table);
			}
		} catch (\Zend_Db_Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}
	}

	private function createActorTable(SchemaSetupInterface $installer)
	{
		try {
			if (!$installer->tableExists('magenest_actor')) {
				$table = $installer->getConnection()->newTable(
					$installer->getTable('magenest_actor')
				)
					->addColumn(
						'actor_id',
						Table::TYPE_INTEGER,
						null,
						[
							'primary' => true,
							'identity' => true,
							'nullable' => false,
							'unsigned' => true,
						],
						'Actor ID'
					)->addColumn(
						'name',
						Table::TYPE_TEXT,
						255,
						['nullable => false'],
						'Actor Name'
					)->addColumn(
						'created_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
						'Created At'
					)->addColumn(
						'updated_at',
						Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
						'Updated At'
					)->setComment('Actor Table');
				$installer->getConnection()->createTable($table);
			}
		} catch (\Zend_Db_Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}
	}

	private function createMovieActorTable(SchemaSetupInterface $installer)
	{
		try {
			if (!$installer->tableExists('magenest_movie_actor')) {
				$table = $installer->getConnection()->newTable(
					$installer->getTable('magenest_movie_actor')
				)
					->addColumn(
						'movie_id',
						Table::TYPE_INTEGER,
						null,
						[
							'primary' => true,
							'nullable' => false,
							'unsigned' => true,
						],
						'Movie ID'
					)->addColumn(
						'actor_id',
						Table::TYPE_INTEGER,
						null,
						[
							'primary' => true,
							'nullable' => false,
							'unsigned' => true,
						],
						'Actor ID'
					)->addForeignKey(
						"fk_movie_movie",
						"movie_id",
						"magenest_movie",
						"movie_id"
					)->addForeignKey(
						"fk_actor_actor",
						"actor_id",
						"magenest_actor",
						"actor_id"
					)->setComment('Movie Actor Table');
				$installer->getConnection()->createTable($table);
			}

		} catch (\Zend_Db_Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}
	}

}
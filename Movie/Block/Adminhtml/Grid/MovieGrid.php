<?php
/**
 * MovieGrid
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Block\Adminhtml\Grid;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magenest\Movie\Model\ResourceModel\Movie\Collection as MovieCollection;

class MovieGrid extends Extended
{
    
    protected $movieCollection;
    
    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        MovieCollection $movieCollection,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->movieCollection = $movieCollection;
        
    }
    
    
    protected function _prepareCollection()
    {
        $this->setCollection($this->movieCollection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        try {
            $this->addColumn(
                'movie_id',
                [
                    'header' => __('ID'),
                    'index'  => 'movie_id',
                ]
            );
            $this->addColumn(
                'name',
                [
                    'header' => __('Movie Name'),
                    'index'  => 'name',
                ]
            );
            $this->addColumn(
                'description',
                [
                    'header' => __('Description'),
                    'index'  => 'description',
                ]
            );
            $this->addColumn(
                'rating',
                [
                    'header' => __('Rating'),
                    'index'  => 'rating',
                ]
            );
            $this->addColumn(
                'director_id',
                [
                    'header' => __('Director ID'),
                    'index'  => 'director_id',
                ]
            );
        } catch (\Exception $e) {
            echo $e->getTraceAsString() . "<br>";
        }
        return $this;
    }

}
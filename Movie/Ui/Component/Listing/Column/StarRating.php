<?php

namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollectionFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;


class StarRating extends Column
{
    protected $movieFactory;
    
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        MovieCollectionFactory $movieFactory,
        array $components = [],
        array $data = []
    ) {
        $this->movieFactory = $movieFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    
    public function prepareDataSource(array $dataSource)
    {
        //$this->movieFactory->create();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                //if ($movie = $this->movieFactory->create($item['movie_id'])) {
                $ratingStr = $this->buildingRating($item['rating']);
                $item['rating'] = html_entity_decode($ratingStr);
                //}
            }
        }
        
        return $dataSource;
    }
    
    private function buildingRating($rating)
    {
        //template
        $check = '<span style="color:orange;" class="fa fa-star"></span>';
        $halfcheck = '<span style="color:orange;" class="fa fa-star-half-o"></span>';
        $uncheck = '<span class="fa fa-star-o"></span>';
        
        $str = '';
        $checkNum = $rating % 2;
        if ($checkNum) {
            $iMax = round($rating / 2) - 1;
            for ($i = 0; $i < $iMax; $i++) {
                $str .= $check;
            }
            $str .= $halfcheck;
            for ($i = 0; $i < 5 - ($iMax + 1); $i++) {
                $str .= $uncheck;
            }
        } else {
            $iMax = round($rating / 2);
            for ($i = 0; $i < $iMax; $i++) {
                $str .= $check;
            }
            for ($i = 0; $i < 5 - $iMax; $i++) {
                $str .= $uncheck;
            }
        }
        
        return $str;
    }
}
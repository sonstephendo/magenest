/**
 * todolist
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
define(
	[
		'ko',
		'jquery',
	],
	function (ko, $) {
		'use strict';
		var task = {
			name: ko.observable(),
			description: ko.observable(),
			priority: ko.observable(),
		};
		var tasks = ko.observableArray();
		
		// var init = function () {
		//     ko.applyBindings(ToDoList);
		// };
		// /* execute the init function when the DOM is ready */
		// $(init);
		
		var addTask = function () {
			var name = task.name();
			// if (tasks.filter(task => (task.name === name))) {
			var names = ko.utils.arrayFilter(tasks(), function (task) {
				return (task.name === name);
			});
			if (!names.length) {
				tasks.push({
					name: task.name(),
					description: task.description(),
					priority: task.priority(),
					status: ko.observable('new'),
				});
			}
		};
		
		var clearTask = function () {
			task.name(null);
			task.description(null);
			task.priority("1");
		};
		
		var deleteTask = function (task) {
			console.log("Deleting task with name: " + task.name);
			//remove the task from the tasks array
			tasks.remove(task);
		};
		
		/* method to complete a task */
		var completeTask = function (task) {
			console.log("Completing task with name: " + task.name);
			//set status of task to complete
			task.status('complete');
		};
		
		/* method to sort the tasks by priority */
		var sortByPriority = function () {
			console.log("Sorting tasks by priority");
			tasks.sort(
				function (left, right) {
					return left.priority === right.priority ?
						0 : (left.priority < right.priority ? -1 : 1);
				});
		};
		
		/* method to sort the tasks by name */
		var sortByName = function () {
			console.log("Sorting tasks by name");
			tasks.sort(
				function (left, right) {
					return left.name === right.name ?
						0 : (left.name < right.name ? -1 : 1);
				});
		};
		/* observable to compute number of completed tasks */
		/* observable to compute number of completed tasks */
		var numOfCompletedTasks = ko.pureComputed(function() {
			var completedTasks = ko.utils.arrayFilter(tasks(), function(task) {
				return task.status() === 'complete';
			});
			return completedTasks.length;
		});
		
		return {
			/* add members that will be exposed publicly */
			tasks: tasks,
			task: task,
			addTask: addTask,
			clearTask: clearTask,
			deleteTask: deleteTask,
			completeTask: completeTask,
			sortByPriority: sortByPriority,
			sortByName: sortByName,
			numOfCompletedTasks: numOfCompletedTasks,
		};
	},
);
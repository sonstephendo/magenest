<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 08:24
 */

namespace Magenest\Movie\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magenest\Movie\Model\MovieActorFactory;
use Magenest\Movie\Model\MovieFactory;
use Magenest\Movie\Model\ActorFactory;
use Magenest\Movie\Model\DirectorFactory;

class UpgradeData implements UpgradeDataInterface
{

	protected $_movieFactory;
	protected $_actorFactory;
	protected $_directorFactory;
	protected $_movieActorFactory;

	public function __construct(
		MovieFactory $movieFactory,
		ActorFactory $actorFactory,
		DirectorFactory $directorFactory,
		MovieActorFactory $movieActorFactory
	)
	{
		$this->_movieFactory = $movieFactory;
		$this->_actorFactory = $actorFactory;
		$this->_directorFactory = $directorFactory;
		$this->_movieActorFactory = $movieActorFactory;
	}

	/**
	 * Upgrades data for a module
	 *
	 * @param ModuleDataSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '1.0.1', '<')) {
			$installer = $setup;
			$installer->startSetup();

			$this->insertSampleData();
//			$this->insertDirectorData();

			$installer->endSetup();
		}
	}

	private function insertDirectorData()
	{
		//		DIRECTOR DATA
		$data = [
			['name' => "director 1"],
			['name' => "director 2"],
			['name' => "director 3"],
		];
		try {
			$director = $this->_directorFactory->create();
			foreach ($data as $item) {
				$director->setData($item)->save();
			}
		} catch (\Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}

	}

	private function insertSampleData()
	{
		//		DIRECTOR DATA
		$data = [
			['name' => "director 1"],
			['name' => "director 2"],
			['name' => "director 3"],
		];
		try {
			$director = $this->_directorFactory->create();
			foreach ($data as $item) {
				$director->setData($item)->save();
			}
		} catch (\Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}


//		ACTOR DATA
		$data = [
			['name' => "actor 1"],
			['name' => "actor 2"],
			['name' => "actor 3"],
			['name' => "actor 4"],
			['name' => "actor 5"],
			['name' => "actor 6"],
			['name' => "actor 7"]
		];

		try {
			$actor = $this->_actorFactory->create();
			foreach ($data as $item)
				$actor->setData($item)->save();
		} catch
		(\Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}

//		MOVIE DATA
		$data = [
			['name' => "movie 1",
				'description' => 'movie1_desc',
				'rating' => 11,
				'director_id' => 2
			],
			['name' => "movie 2",
				'description' => 'movie2_desc',
				'rating' => 19,
				'director_id' => 1],
			['name' => "movie 3",
				'description' => 'movie3_desc',
				'rating' => 18,
				'director_id' => 3
			],
			['name' => "movie 4",
				'description' => 'movie4_desc',
				'rating' => 10,
				'director_id' => 3
			]];

		try {
			$movie = $this->_movieFactory->create();
			foreach ($data as $item)
				$movie->setData($item)->save();
		} catch
		(\Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}

//		ACTOR DATA
		$data = [
			[
				'movie_id' => 1,
				'actor_id' => 1
			],
			[
				'movie_id' => 1,
				'actor_id' => 2
			],
			[
				'movie_id' => 2,
				'actor_id' => 3
			],
			[
				'movie_id' => 2,
				'actor_id' => 4
			],
			[
				'movie_id' => 2,
				'actor_id' => 5
			],
			[
				'movie_id' => 3,
				'actor_id' => 5
			],
			[
				'movie_id' => 1,
				'actor_id' => 6
			],
			[
				'movie_id' => 3,
				'actor_id' => 6
			],
			[
				'movie_id' => 1,
				'actor_id' => 7
			],
			[
				'movie_id' => 4,
				'actor_id' => 7
			],
			[
				'movie_id' => 4,
				'actor_id' => 4
			]
		];

		try {
			$movieActor = $this->_movieActorFactory->create();
			foreach ($data as $item) {
				$movieActor->setData($item)->save();
			}
		} catch (\Exception $e) {
			echo $e->getTraceAsString() . "<br>";
		}

	}
}
<?php
/**
 * CartPlugin
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Plugin;

use Magento\Quote\Model\Quote\Item;

use Magento\Catalog\Model\ProductFactory;
class AddToCartPlugin
{
    /**
     * @var OutputInterface
     */
    private $output;
    /**
     * @var ProductRepositoryInterfaceFactory
     */
    private $productInterfaceFactory;
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $image;
    
    public function __construct(
        ProductFactory $productFactory,
        \Magento\Catalog\Helper\Image $image
    ) {
        $this->productFactory = $productFactory;
        $this->image = $image;
    }
    
    public function aroundGetItemData($subject, callable $proceed, Item $item)
    {
        $result = $proceed($item);
        $key = key($item->getData('qty_options'));
        //product child
        $product = $this->productFactory->create()->load($key);
        //image
        $url = $this->image->init($product, 'product_small_image')->getUrl();
        
        $result['product_name'] = $result['product_sku'];
        $result['product_image']['src'] = $url;
        return $result;
    }
}
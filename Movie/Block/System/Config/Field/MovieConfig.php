<?php
/**
 * Disable
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Block\System\Config\Field;


use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;

class MovieConfig extends Field
{
    /**
     * @var MovieFactory
     */
    private $movieFactory;
    
    public function __construct(
        Context $context,
        CollectionFactory $movieFactory,
        array $data = []
    ) {
        
        parent::__construct(
            $context, $data
        );
        $this->movieFactory = $movieFactory;
    }
    
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('readonly', 1);
        $data = $this->movieFactory->create()->getData();
        $element->setData('value', sizeof($data));
        return $element->getElementHtml();
    }
}
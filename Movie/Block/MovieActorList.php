<?php

namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Movie;
use Magento\Framework\View\Element\Template;
use Magenest\Movie\Model\MovieActorFactory;
use Magenest\Movie\Model\ActorFactory;
use Magento\Framework\View\Element\Template\Context;

class MovieActorList extends Template
{
    protected $movieActorFactory;
    protected $_actorFactory;

    /**
     * Construct
     * @param Context           $context
     * @param MovieActorFactory $movieActorFactory
     * @param ActorFactory      $actorFactory
     * @param array             $data
     */
    public function __construct(
        Context $context,
        MovieActorFactory $movieActorFactory,
        ActorFactory $actorFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->movieActorFactory = $movieActorFactory;
        $this->_actorFactory = $actorFactory;
    }

}
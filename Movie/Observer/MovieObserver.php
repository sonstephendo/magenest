<?php
/**
 * Customer
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MovieObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $movie = $observer->getData('movie');
        $movie->setRating(0);
    }
}
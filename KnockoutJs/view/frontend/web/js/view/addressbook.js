/**
 * skeleton
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
define([
    'uiComponent',
    'ko',
    'jquery'
], function (Component, ko, $) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();
        },

        contact: {
            name: ko.observable(),
            phoneNumber: ko.observable()
        },

        contacts: ko.observableArray(),

        addContact: function () {
            var self = this;
            console.log("Adding new contact with name: " + self.contact.name() + " and phone number: " + self.contact.phoneNumber());
            //add the contact to the contacts array
            if (self.contact.name() || self.contact.phoneNumber()) {
                self.contacts.push({
                    name: self.contact.name(),
                    phoneNumber: self.contact.phoneNumber()
                });
            }
        },

        clearContact: function () {
            var self = this;
            self.contact.name(null);
            self.contact.phoneNumber(null);
        }

    });
});
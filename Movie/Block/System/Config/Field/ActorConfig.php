<?php
/**
 * ActorConfig
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Block\System\Config\Field;


use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;

class ActorConfig extends Field
{
    
    /**
     * @var ActorFactory
     */
    private $actorFactory;
    
    public function __construct(
        Context $context,
        CollectionFactory $actorFactory,
        array $data = [])
    {
        
        parent::__construct(
            $context, $data
        );
    
        $this->actorFactory = $actorFactory;
    }
    
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('readonly', 1);
        $data = $this->actorFactory->create()->getData();
        $element->setData('value', sizeof($data));
        return $element->getElementHtml();
    }
}
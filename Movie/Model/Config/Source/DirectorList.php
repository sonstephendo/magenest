<?php
/**
 * DirectorMap
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Model\Config\Source;


use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class DirectorList implements ArrayInterface
{
    private $collectionFactory;
    
    /** @var array */
    private $options;
    
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->options = $this->buildDirectors();
    }
    
    public function toOptionArray()
    {
        return $this->options;
    }
    
    private function buildDirectors()
    {
        $array = [];
        $temp = $this->collectionFactory->create()->getItems();
        foreach ($temp as $item) {
            array_push(
                $array, [
                'value' => $item->getDirectorId(),
                'label' => __($item->getDirectorId() . ' - '.  $item->getName())
            ]
            );
        }
        return $array;
    }
}
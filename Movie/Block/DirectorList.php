<?php

namespace Magenest\Movie\Block;

use Magenest\Movie\Model\DirectorFactory;
use Magento\Framework\View\Element\Template;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory as ActorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\MovieActor\CollectionFactory as MovieActorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollectionFactory;
use Magento\Framework\View\Element\Template\Context;

class DirectorList extends Template
{
    protected $directorCollectionFactory;
    /**
     * @var ActorCollectionFactory
     */
    private $_actorCollectionFactory;
    /**
     * @var MovieActorCollectionFactory
     */
    private $_movieActorCollectionFactory;

    private $_movieCollectionFactory;


    /**
     * Construct
     * @param Context                     $context
     * @param DirectorCollectionFactory   $directorCollectionFactory
     * @param ActorCollectionFactory      $actorCollectionFactory
     * @param MovieActorCollectionFactory $movieActorCollectionFactory
     * @param MovieCollectionFactory      $movieCollectionFactory
     * @param array                       $data
     */
    public function __construct(
        Context $context,
        DirectorCollectionFactory $directorCollectionFactory,
        ActorCollectionFactory $actorCollectionFactory,
        MovieActorCollectionFactory $movieActorCollectionFactory,
        MovieCollectionFactory $movieCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_directorCollectionFactory = $directorCollectionFactory;
        $this->_actorCollectionFactory = $actorCollectionFactory;
        $this->_movieActorCollectionFactory = $movieActorCollectionFactory;
        $this->_movieCollectionFactory = $movieCollectionFactory;
    }


    public function getDirector()
    {
        $collection = $this->_directorCollectionFactory->create();
        $collection->addFieldToFilter('name', ['name' => 'director2']);

//		$collection2 = $this->_actorCollectionFactory->create();
        $collection2 = $this->_directorCollectionFactory->create();
        $collection4 = $this->_movieCollectionFactory->create();
        return $collection;
    }
}
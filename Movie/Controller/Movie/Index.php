<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 14:51
 */

namespace Magenest\Movie\Controller\Movie;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;


class Index extends Action
{
	/**
	 * @param Context $context
	 * @param PageFactory $resultPageFactory
	 */
	protected $resultPageFactory;
 
	public function __construct(
		Context $context,
		PageFactory $resultPageFactory
	)
	{
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}
 
	public function execute()
	{
		return $this->resultPageFactory->create();
	}
 
}
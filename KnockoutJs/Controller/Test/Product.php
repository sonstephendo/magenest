<?php
/**
 * Product
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\KnockoutJs\Controller\Test;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Helper\Image;
use Magento\Store\Model\StoreManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Product extends \Magento\Framework\App\Action\Action
{
	protected $productFactory;
	protected $imageHelper;
	protected $_storeManager;
	
	protected $listProduct;
	
	public function __construct(
		Context $context,
		\Magento\Framework\Data\Form\FormKey $formKey,
		ProductFactory $productFactory,
		StoreManager $storeManager,
		Image $imageHelper
	) {
		parent::__construct($context);
		$this->productFactory = $productFactory;
		$this->_storeManager = $storeManager;
		$this->imageHelper = $imageHelper;
	}
	
	public function getCollection()
	{
		return $this->productFactory->create()
			->getCollection()
			->addAttributeToSelect('*')
			->setPageSize(5)
			->setCurPage(1);
	}
	
	
	public function execute()
	{
		if ($id = $this->getRequest()->getParam('id')) {
			$product = $this->productFactory->create()->load($id);
			$productData = [
				'entity_id' => $product->getId(),
				'name'      => $product->getName(),
				'price'     => '$' . $product->getPrice(),
				'src'       => $this->imageHelper->init($product, 'product_base_image')->getUrl(),
			];
			echo json_encode($productData);
			return;
		}
	}
}
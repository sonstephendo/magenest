define([
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal',
    'Magento_Ui/js/modal/alert',
], function (Component, $, modal, alert) {
    //js mode
    'use strict';

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            return this;
        },
        initClick: function () {

            $('.message a').click(function () {
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            });
            //First button
            $('#button-first').on('click', function () {
                alert({
                    title: 'Alert!',
                    content: 'Hello World!',
                    actions: {
                        always: function () {
                        }
                    }
                });
            }, null);

            //Second button
            var options = {
                type: 'popup',
                title: 'Login Modal',
                responsive: true,
                innerScroll: false,
                modalClass: 'custom_modal_class',
                clickableOverlay: true,
                buttons: []
            };

            $('#button-second').on('click', function () {
                var popup = $('.modal-modal');
                modal(options, popup);
                popup.modal('openModal');
            }, null);


        }
    });
});

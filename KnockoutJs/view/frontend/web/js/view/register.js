/**
 * todolist
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
define([
    'uiComponent',
    'ko',
    'jquery',
    'Magenest_KnockoutJs/js/model/RegistrationForm',
    'mage/mage',
], function (Component, ko, $, Form) {
    'use strict';
    var self;
    return Component.extend({
        initialize: function () {
            self = this;
            this._super();
            Form.addCreditCard();
        },
	    Form: Form,
        initForm: function () {
	        var dataForm = $('#custom-form');
	        dataForm.mage('validation', {});
        }
        
    });
});
<?php
/**
 * Save
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Controller\Adminhtml\Movie;


use Magenest\Movie\Model\MovieFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Message\ManagerInterface;

class Save extends Action
{
    
    /**
     * @var MovieFactory
     */
    private $movieFactory;
    
    public function __construct(
        Context $context,
        MovieFactory $movieFactory,
        ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->movieFactory = $movieFactory;
        $this->messageManager = $messageManager;
    }
    
    public function execute()
    {
        try {
            $movie = $this->movieFactory->create()
                ->setData($this->getRequest()->getPostValue()['movie_form']);
            
            $this->_eventManager->dispatch('save_a_movie', ['movie' => $movie]);
            $movie->save();
            
            $this->messageManager->addSuccess('You saved the movie.');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->resultRedirectFactory->create()->setPath('*/*/new');
        }
        return $this->resultRedirectFactory->create()->setPath('movie/movie/index');
    }
    
    
}
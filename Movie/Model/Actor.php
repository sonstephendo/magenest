<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 15:35
 */

namespace Magenest\Movie\Model;


use Magenest\Movie\Api\Data\MovieInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Actor extends AbstractModel implements MovieInterface, IdentityInterface
{
	// constant place
	const ID = 'actor_id';
	const CACHE_TAG = 'magenest_actor';


	protected function _construct()
	{
		$this->_init('Magenest\Movie\Model\ResourceModel\Actor');
	}

	public function getId()
	{
		return $this->_getData(self::ID);
	}

	public function getName()
	{
		return $this->_getData(self::NAME);
	}

	public function getCreationTime()
	{
		return $this->_getData(self::CREATION_TIME);
	}

	public function getUpdateTime()
	{
		return $this->_getData(self::UPDATE_TIME);
	}

	/**
	 * Return unique ID(s) for each object in system
	 *
	 * @return string[]
	 */
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
}
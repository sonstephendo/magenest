define([
    'ko',
    'uiComponent',
    'mage/url',
    'jquery',
    'Magento_Ui/js/modal/alert'
], function (ko, Component, urlBuilder, $, alert) {
    'use strict';
    return Component.extend({
        id: 1,
        productList: ko.observableArray([]),
        getProduct: function () {
            var self = this;
            $.ajax({
                type: 'GET',
                url: urlBuilder.build('knockout/test/product?id=' + self.id),
                data: {},
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.entity_id) {
                        self.productList.push(res);
                        console.log(self.productList);
                        self.id++;
                    } else {
                        alert({
                            title: 'Error!',
                            content: 'Product not found !',
                            actions: {
                                always: function () {
                                }
                            }
                        });
                    }
                    console.log(res);
                },
                error: function (res) {
                    console.log(res);
                }
            });
        }
    });
});
<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 15:35
 */

namespace Magenest\Movie\Model;


use Magenest\Movie\Api\Data\MovieInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Movie extends AbstractModel implements MovieInterface, IdentityInterface
{
	const MOVIE_ID = 'movie_id';
	const DIRECTOR_ID = 'director_id';
	const DESCRIPTION = 'description';
	const RATING = 'rating';
	const CACHE_TAG = 'magenest_movie';


	protected function _construct()
	{
		$this->_init('Magenest\Movie\Model\ResourceModel\Movie');
	}

	public function getId()
	{
		return $this->_getData(self::MOVIE_ID);
	}

	public function getName()
	{
		return $this->_getData(self::NAME);
	}

	public function getCreationTime()
	{
		return $this->_getData(self::CREATION_TIME);
	}

	public function getUpdateTime()
	{
		return $this->_getData(self::UPDATE_TIME);
	}

	public function getDirectorId()
	{
		return $this->_getData(self::DIRECTOR_ID);
	}

	public function getDescription()
	{
		return $this->_getData(self::DESCRIPTION);
	}

	public function getRating()
	{
		return $this->_getData(self::RATING);
	}

	/**
	 * Return unique ID(s) for each object in system
	 *
	 * @return string[]
	 */
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
}
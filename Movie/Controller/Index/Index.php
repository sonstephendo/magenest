<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 14:51
 */

namespace Magenest\Movie\Controller\Index;

use Magenest\Movie\Model\MovieActor;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    //**	@var \Magento\Framework\View\Result\PageFactory		*/
    private $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->collectionExecute();
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    private function collectionExecute()
    {
        /**
         * @var $model \Magenest\Movie\Model\MovieActor
         */
        $model = $this->_objectManager->create(MovieActor::class);
        $collection = $model->getCollection()->addFieldToFilter('movie_id', ['eq' => '1']);
        /**
         * @var $item \Magenest\Movie\Model\MovieActor
         */
        foreach ($collection as $item) {
            var_dump($item->getData());
        }
        die('test');
    }
}
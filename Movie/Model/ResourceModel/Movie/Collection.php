<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 15:45
 */

namespace Magenest\Movie\Model\ResourceModel\Movie;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected $_idFieldName = 'movie_id';

	public function _construct()
	{
		$this->_init(
			'\Magenest\Movie\Model\Movie',
			'\Magenest\Movie\Model\ResourceModel\Movie');
	}
}
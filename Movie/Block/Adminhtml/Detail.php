<?php
/**
 * Detail
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Block\Adminhtml;


use Magento\Backend\Block\Template\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;

class Detail extends \Magento\Backend\Block\Template
{
    /**
     * @var ModuleList
     */
    protected $moduleList;
    
    protected $customerCollection;
    protected $productCollection;
    protected $orderCollection;
    protected $orderFactory;
    protected $invoiceCollection;
    protected $creditmemoCollection;
    
    public function __construct(
        Context $context,
        ModuleListInterface $moduleList,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollection,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollection,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditmemoCollection
    ) {
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->moduleList = $moduleList;
        parent::__construct($context);
        
        $this->customerCollection = $customerCollection;
        $this->productCollection = $productCollection;
        $this->orderCollection = $orderCollection;
        $this->invoiceCollection = $invoiceCollection;
        $this->creditmemoCollection = $creditmemoCollection;
        
    }
    
    public function getNumberOfModules()
    {
        $data = $this->moduleList->getAll();
        return sizeof($data);
    }
    
    public function getNumberOfModulesByVendor()
    {
        $data = $this->moduleList->getAll();
        $vendors = [];
        foreach ($data as $index => $value) {
            if (strpos($index, 'Magento',0) !== 0) {
                $vendors[] = $index;
            }
        }
        return sizeof($vendors);
    }
    
    public function getMagentoData()
    {
        return [
            'customerNum' => $this->getNumberOfRecord($this->customerCollection),
            'productNum' => $this->getNumberOfRecord($this->productCollection),
            'orderNum' => $this->getNumberOfRecord($this->orderCollection),
            'invoiceNum' => $this->getNumberOfRecord($this->invoiceCollection),
            'creditmemosNum' => $this->getNumberOfRecord($this->creditmemoCollection),
        ];
    }
    
    public function getNumberOfRecord($collection)
    {
        return $collection->create()->getSize();
    }
}
<?php
/**
 * Baam
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\KnockoutJs\Block;


use Magento\Framework\View\Element\Template;

class Baam extends Template
{
	public function __construct(
		Template\Context $context,
		array $data = []
	)
	{
		parent::__construct($context, $data);
	}
}
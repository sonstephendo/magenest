<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 15:35
 */

namespace Magenest\Movie\Model;


use Magento\Framework\Model\AbstractModel;

class MovieActor extends AbstractModel
{
	// constant place
	const MOVIE_ID = 'movie_id';
	const ACTOR_ID = 'actor_id';

	protected function _construct()
	{
		$this->_init('\Magenest\Movie\Model\ResourceModel\MovieActor');
	}

	public function getMovieId()
	{
		return $this->_getData(self::MOVIE_ID);
	}

	public function getActorId()
	{
		return $this->_getData(self::ACTOR_ID);
	}
}
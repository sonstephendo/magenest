<?php
/**
 * ConfigObserver
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ConfigObserver implements ObserverInterface
{
    
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $config = $observer->getData('config_data');
        $field = $config->getData('field');
        $value = $config->getData('value');
        if ($field === 'text_field' && $value === 'Ping') {
            $config->setValue('Pong');
        }
    }
}
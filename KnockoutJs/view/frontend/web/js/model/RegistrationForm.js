/**
 * RegisterForm
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
define([
	'ko',
	'jquery',
], function (ko, $) {
	'use strict';
	
	/* extender for required fields */
	ko.extenders.required = function(target, option) {
//observables to indicate an error
		target.hasError = ko.observable(false);
//set the error flag whenever the value changes
		target.subscribe(function (newValue) {
			target.hasError(newValue ? false : true);
		});
//return the original observable
		return target;
	};
	
	var customer = {
		personalInfo: {
			title: ko.observable(),
			firstName: ko.observable().extend({ required: null}),
			middleName: ko.observable(),
			lastName: ko.observable(),
		},
		contactDetails: {
			phoneNumber: ko.observable(),
			emailAddress: ko.observable(),
			preferredContact: ko.observable(),
		},
		address: {
			residential: {
				street: ko.observable(),
				city: ko.observable(),
				postCode: ko.observable(),
				country: ko.observable(),
			},
			postal: {
				type: ko.observable(),
				streetAddress: {
					street: ko.observable(),
					city: ko.observable(),
					postCode: ko.observable(),
					country: ko.observable(),
				},
				poBoxAddress: {
					poBox: ko.observable(),
					city: ko.observable(),
					postCode: ko.observable(),
					country: ko.observable(),
				},
			},
		},
		creditCards: ko.observableArray(),
		interests: ko.observableArray(),
	};
	
	var addCreditCard = function () {
		customer.creditCards.push({
			name: ko.observable(),
			number: ko.observable(),
			expiryDate: ko.observable(),
		});
	};
	
	var submit = function () {
		console.log(ko.toJSON(customer));
		console.log("The form is submitted");
	};
	
	/* options for the title drop down*/
	var titleOptions = [
		{
			value: 'Mr',
			setTitle: function () {
				customer.personalInfo.title("Mr");
			},
		},
		{
			value: 'Mrs',
			setTitle: function () {
				customer.personalInfo.title("Mrs");
			},
		},
		{
			value: 'Miss',
			setTitle: function () {
				customer.personalInfo.title("Miss");
			},
		},
		{
			value: 'Dr',
			setTitle: function () {
				customer.personalInfo.title("Dr");
			},
		},
	];
	
	/* computed observable for title drop down text change */
	var titleSelect = ko.pureComputed(function () {
		if (customer.personalInfo.title() == null) {
			return "select";
		} else {
			return customer.personalInfo.title();
		}
	});
	
	/* method to delete a credit card from the credit cards array */
	var deleteCreditCard = function (card) {
		console.log("Deleting credit card with number: " +
			card.number());
//remove the credit card from the array
		customer.creditCards.remove(card);
	};
	
	/* method to traverse the model and clear observables */
	var traverseAndClearModel = function (jsonObj) {
		$.each(jsonObj, function (key, val) {
			if (ko.isObservable(val)) {
				if (val.removeAll !== undefined) {
					val.removeAll();
				} else {
					val(null);
				}
			} else {
				traverseAndClearModel(val);
			}
		});
	};
	
	/* clear the model */
	var clear = function () {
		console.log("Clear customer model");
		traverseAndClearModel(customer);
		//add the first credit card
		addCreditCard();
	};
	
	
	return {
		submit: submit,
		customer: customer,
		titleOptions: titleOptions,
		titleSelect: titleSelect,
		addCreditCard: addCreditCard,
		deleteCreditCard: deleteCreditCard,
		clear: clear,
		
	};
	
});
<?php
/**
 * Avatar
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Avatar\Block\Account\Dashboard;


class Avatar extends \Magento\Framework\View\Element\Template
{
    private $currentCustomer;
    private $_helperView;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Customer\Helper\View $helperView,
        array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->_helperView = $helperView;
        parent::__construct($context, $data);
    }
    
    public function getName()
    {
        return $this->_helperView->getCustomerName($this->getCustomer());
    }
    
    public function getCustomer()
    {
        $customer = $this->currentCustomer->getCustomer();
        
        return $customer;
    }
    
    public function getAvatarSrc()
    {
        $customer = $this->getCustomer();
        $location = $customer->getCustomAttribute('avatar')->getValue();
        $url = $this->getUrl() . 'pub/media/customer' . $location;
        return $url;
    }
    
    public function getPhoneAddress()
    {
        //
        $phones = [];
        $addresses = $this->currentCustomer->getCustomer()->getAddresses();
        foreach ($addresses as $address) {
            $phones[] = $address->getTelephone();
        }
        return $phones;
    }
}
/**
 * learning
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */
// module Uppercase:
var Module = (function () {
    /* private attribute */
    var privateAttribute;
    /* private function */
    var privateFunction = function () {
    };
    /* public attribute */
    var publicAttribute;
    /* public function */
    var publicFunction = function () {
    };

    var init = function () {
        /* Module initialization logic*/
    };
    /* fire the init function */
    init();

    /* return object with reference to public attributes and functions */
    return {
        publicAttribute: publicAttribute,
        publicFunction: publicFunction
    };
})();

Module.publicAttribute = 'foo';
Module.publicFunction();
$(init);
// construct and execute the anonymous function

// Using the module with view model
var ContactViewModel = (function () {
    var contact = {
        id: ko.observable(1),
        name: ko.observable('John'),
        phoneNumber: ko.observable(11110000)
    };

    var retrieveContact = function () {

    };
    var updateContact = function (newPhoneNumber) {

    };

    var init = function () {

    };

    $(init);

    return {
        contact: contact,
        updateContact: updateContact
    };
})();

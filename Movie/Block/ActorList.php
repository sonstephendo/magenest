<?php

namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;

class ActorList extends Template
{
	protected $_actorCollection;

	/**
	 * Construct
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @var \Magenest\Movie\Model\ResourceModel\Actor\Collection
	 * @param array $data
	 */
	public function __construct(
		Template\Context $context,
		\Magenest\Movie\Model\ResourceModel\Actor\Collection $actorCollection,
		array $data = [])
	{
		parent::__construct($context, $data);
		$this->_actorCollection = $actorCollection;
	}


	public function getList()
	{
		if (!$this->hasData('actors')) {
			$actors = $this->_actorCollection->load();
//				->addOrder(
//					PostInterface::CREATION_TIME,
//					PostCollection::SORT_ORDER_DESC

			$this->setData('actors', $actors);
		}
		return $this->getData('actors');
	}
}
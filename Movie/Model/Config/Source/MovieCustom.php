<?php
/**
 * Custom
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Model\Config\Source;


use Magento\Framework\Option\ArrayInterface;

class MovieCustom implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => '1',
                'label' => __('Show')
            ],
            [
                'value' => '2',
                'label' => __('Not-Show')
            ]
        ];
    }
}
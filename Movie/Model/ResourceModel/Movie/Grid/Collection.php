<?php
/**
 * Collection
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Model\ResourceModel\Movie\Grid;


use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Psr\Log\LoggerInterface as Logger;

class Collection extends SearchResult
{
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'magenest_movie',
        $resourceModel = 'Magenest\Movie\Model\ResourceModel\Movie',
        $identifierName = null,
        $connectionName = null
    )
    {
        parent::__construct(
            $entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel,
            $identifierName, $connectionName
        );
    }
}
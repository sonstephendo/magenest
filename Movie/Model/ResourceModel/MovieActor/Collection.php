<?php
/**
 * Created by PhpStorm.
 * User: sonstephendo
 * Date: 27/06/2018
 * Time: 15:45
 */

namespace Magenest\Movie\Model\ResourceModel\MovieActor;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	public function _construct()
	{
		$this->_init(
			'\Magenest\Movie\Model\MovieActor',
			'\Magenest\Movie\Model\ResourceModel\MovieActor');
	}
}
<?php
/**
 * Index
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\KnockoutJs\Controller\Baam;


use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
	
	protected $resultPageFactory;
	
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}
	
	public function execute()
	{
		return $this->resultPageFactory->create();
		//return $this->resultFactory->create(ResultFactory::TYPE_RAW);
	}
	
}
<?php
/**
 * Actor
 *
 * @copyright Copyright © 2018 Magenest. All rights reserved.
 * @author    sonstephendo@gmail.com
 */

namespace Magenest\Movie\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Actor extends Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Movie';
        $this->_controller = 'adminhtml_actor';
        $this->_headerText = __('Actors');
        $this->_addButtonLabel = __('New Actor');
        parent::_construct();
    }
}